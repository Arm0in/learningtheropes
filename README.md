![Tux, the Linux mascot](https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg)
# Learning the Ropes of ***Markdown***  

1. ## What is markdown?
Markdown is a plain text formatting syntax aimed at making writing for the internet easier. The philosophy behind Markdown is that plain text documents should be readable without tags mussing everything up, but there should still be ways to add text modifiers like lists, bold, italics, etc. It is an alternative to WYSIWYG (what you see is what you get) editors, which use rich text that later gets converted to proper HTML.

It’s possible you’ve encountered Markdown without realizing it. Facebook chat, Skype, and Reddit all let you use different flavors of Markdown to format your messages.

Here’s a quick example: to make words bold using Markdown, you simply enclose them in * (asterisks). So, *bold word* would look like bold word when everything is said and done.

All told, Markdown is a great way to write for the web using plain text. :smile:


2. ## Drawing diagrams using Mermaid:

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggTFJcbkEoTWFzdGVyKSAtLT5CKGNvbW1pdDEgKVxuICAgIEIgLS0-IEMoY29tbWl0MilcbiAgICBDIC0tPnxPbmV8IEQoTWFzdGVyIGJyYW5jaClcbiAgICBDIC0tPnxUd298IEVbYnJhbmNoIHR3b11cbiIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggTFJcbkEoTWFzdGVyKSAtLT5CKGNvbW1pdDEgKVxuICAgIEIgLS0-IEMoY29tbWl0MilcbiAgICBDIC0tPnxPbmV8IEQoTWFzdGVyIGJyYW5jaClcbiAgICBDIC0tPnxUd298IEVbYnJhbmNoIHR3b11cbiIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)  

| I | Am |
| ----------- | ----------- |
| Armin | Barghi |
